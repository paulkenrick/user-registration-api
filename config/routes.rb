Rails.application.routes.draw do
  namespace :v1 do
    resources :users, only: [:create] do
      collection do
        get "/token" => "users#token"
        get "/details" => "users#details"
        get "/reset" => "users#reset"
        put "/assign" => "users#assign"
      end
    end

    resources :groups, only: [:create]
  end
end
