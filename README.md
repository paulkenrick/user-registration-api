# User Registration API

This application is a simple REST-ful API providing JSON response to meet the following instructions:

*Create a Ruby on Rails based application for a user registration and authentication system. It should be designed as a REST-ful API with JSON only responses, there is no need for views.*

*Please incorporate the following requirements:*

*- Users need to be able to register with a username, email address and password (there is no need to send a confirmation email).*

*- Users need to be able to authenticate with their username and password.*

*- When a user is successfully authenticated, the API needs to respond with a unique user token.*

*- User tokens are valid for 1 hour.*

*- The API also needs to allow a user token that is up to 2 hours old to be exchanged for a fresh user token.*

*- Groups have a unique name and can have many users and users can belong to many groups.*

*- Some users have admin privileges and they need to be able to carry out the following actions using a valid user token:*
  - *Create groups.*
  - *Assign users to groups.*

*- With a valid user token, other applications need to be able to supply a username or email address to request the user’s details (username, email address and group memberships).*


## Getting Started

1. Clone the project
2. Install the required gems by running `bundle install`
3. Start a local server by running `bundle exec rails s -p 3000`
4. Begin making requests to the API's endpoints (these are described further below)

## Running the tests

I have include full model and request specs.  To run these, simply run `bundle exec rspec`.

## Valid Endpoints

| Required Action        | Endpoint           | Params  | Headers | Available to admins only? |
| ------------- |-------------| -----| ---| ---|
| Create a user      | /v2/users (POST) | username - string<br>email - string<br>password - string<br>password_confirmation - string | | false |
|Obtain a token      | /v2/users/token (GET) |  | token - string | false |
|Obtain user details | /v2/users/details (GET) | identifier* - string | token - string | false |
| Reset a token    | /v2/users/reset (GET) |  | token - string | false |
| Assign a user to a group | /v2/users/assign (PUT) | identifier* - string<br>group_name - string | token - string | true |
| Create a group    | /v2/groups (POST) | name - string | token - string | true |

*'identifier' param requires either a user's username or email address

## Notes from the Author

This API took me approximately 6 hours to build (including tests and documentation).  I was told that completing the project entirely was not necessary, however I enjoyed doing it so I continued.

The API contains just two models - User and Group - and two corresponding controllers.

I used RSpec for testing, including unit and request specs.  The unit tests include only validation tests; if I had a bit more time I would also include some tests on the instance methods contained in the User model.

I note that tokens are stored in the database in plain text (which is how I have dealt with these type of tokens in the past in my work), but these could be hashed if the data is sensitive.
