class ApplicationController < ActionController::API

  def authenticate_token
    token = request.headers["token"]
    @user = User.find_by(token: token)

    if token.nil? || @user.nil?
      render json: { message: "Invalid token" }, status: 401
    elsif !@user.token_valid?(action_name)
      render json: { message: "Expired token" }, status: 401
    end
  end

  def require_admin_user
    unless @user.admin
      render json: { message: "Permission denied" }, status: 403
    end
  end

end
