class V1::GroupsController < ApplicationController
  before_action :authenticate_token, only: [:create]
  before_action :require_admin_user, only: [:create]

  def create
    group = Group.new(name: params[:name])

    if group.save
      render json: { message: "Group created successfully" }, status: 200
    else
      render json: { message: group.errors.full_messages.join(', ') }, status: 400
    end
  end
end
