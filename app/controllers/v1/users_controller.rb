class V1::UsersController < ApplicationController
  before_action :authenticate_token, only: [:details, :reset, :assign]
  before_action :require_admin_user, only: [:assign]

  def create
    user = User.new(user_params)

    if user.save
      render json: { message: "User created successfully" }, status: 200
    else
      render json: { message: user.errors.full_messages.join(', ') }, status: 400
    end
  end

  def token
    user = User.find_by(username: params[:username])

    if user.nil?
      render json: { message: "Could not find user" }, status: 404
      return
    end

    if user.authenticate(params[:password])
      render json: { token: user.generate_token }, status: 200
    else
      render json: { message: "Invalid credentials" }, status: 401
    end
  end

  def details
    user = User.find_by(email: params[:identifier]) || User.find_by(username: params[:identifier])
    if user
      render json: { username: user.username, email: user.email, groups: user.groups.map(&:name) }, status: 200
    else
      render json: { message: "Could not find user" }, status: 404
    end
  end

  def reset
    render json: { token: @user.generate_token }, status: 200
  end

  def assign
    user = User.find_by(email: params[:identifier]) || User.find_by(username: params[:identifier])
    if user.nil?
      render json: { message: "Could not find user" }, status: 404
      return
    end

    group = Group.find_by(name: params[:group_name])
    if group.nil?
      render json: { message: "Could not find group" }, status: 404
      return
    end

    if user.groups.include?(group)
      render json: { message: "User already belongs to group" }, status: 409
    else
      user.groups << group
      render json: { message: "User successfully assigned to group" }, status: 200
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
