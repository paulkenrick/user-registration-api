class User < ApplicationRecord
  has_secure_password

  has_and_belongs_to_many :groups

  validates :username, presence: true
  validates :email, presence: true, uniqueness: true

  def generate_token
    self.update(token: SecureRandom.urlsafe_base64, last_token_created_at: Time.current)
    self.token
  end

  def token_valid?(action_name)
    validity_length = if action_name == 'reset'
      2.hours
    else
      1.hour
    end

    return false if self.token.nil?
    return false if self.last_token_created_at < validity_length.ago
    true
  end
end
