FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "Test User #{n}" }
    sequence(:email) { |n| "testuser#{n}@example.com" }
    password { "password" }
    token { "123456789" }
    last_token_created_at { Time.now }
    admin { false }
  end
end
