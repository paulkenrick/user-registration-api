require 'rails_helper'

RSpec.describe Group, type: :model do
  it 'has a valid factory' do
    expect(create(:group)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:group, name: nil)).to_not be_valid
  end

  it 'is invalid if name is not unique' do
    group = create(:group)
    expect(build(:group, name: group.name)).to_not be_valid
  end
end
