require 'rails_helper'

RSpec.describe 'User', type: :request do

  describe 'POST /v1/users' do
    context 'when valid parameters are provided' do
      let(:valid_attributes) { { username: "test_user", email: "test_user@example.com", password: 'password', password_confirmation: 'password' } }

      it 'saves a new user' do
        post '/v1/users', params: { user: valid_attributes }
        expect(User.count).to eq(1)
      end

      it 'returns a success message' do
        post '/v1/users', params: { user: valid_attributes }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("User created successfully")
      end
    end

    context 'when invalid parameters are provided' do
      let(:invalid_attributes) { { username: nil, email: "test_user@example.com", password: 'password', password_confirmation: 'password' } }

      it 'does not save a new user' do
        post '/v1/users', params: { user: invalid_attributes }
        expect(User.count).to eq(0)
      end

      it 'returns an error message' do
        post '/v1/users', params: { user: invalid_attributes }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Username can't be blank")
      end
    end
  end

  describe 'GET /v1/users/token' do
    let(:user) { create(:user) }

    context 'when valid username and password provided' do
      it 'returns a token' do
        get '/v1/users/token', params: { username: user.username, password: user.password }
        response_body = JSON.parse(response.body)
        expect(response_body["token"]).to_not eq(nil)
      end
    end

    context 'when valid username or password provided' do
      it 'does not return a token' do
        get '/v1/users/token', params: { username: user.username, password: 'incorrect_password' }
        response_body = JSON.parse(response.body)
        expect(response_body["token"]).to eq(nil)
      end

      it 'returns an error message' do
        get '/v1/users/token', params: { username: user.username, password: 'incorrect_password' }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Invalid credentials")
      end
    end
  end

  describe 'GET /v1/users/details' do
    let(:user) { create(:user) }

    context 'when no token provided' do
      it 'does not return user details' do
        get '/v1/users/details', params: { identifier: user.username }
        response_body = JSON.parse(response.body)
        expect(response_body["username"]).to eq(nil)
      end

      it 'returns an error message' do
        get '/v1/users/details', params: { identifier: user.username }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Invalid token")
      end
    end

    context 'when expired token provided (i.e. more than 1 hour old)' do
      let(:user) { create(:user, last_token_created_at: Time.now - 2.hours) }

      it 'does not return user details' do
        get '/v1/users/details', params: { identifier: user.username }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["username"]).to eq(nil)
      end

      it 'returns an error message' do
        get '/v1/users/details', params: { identifier: user.username }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Expired token")
      end
    end

    context 'when authenticated (i.e. valid token provided)' do
      context 'when invalid user identifier provided' do
        it 'returns an error message' do
          get '/v1/users/details', params: { identifier: 'invalid_username' }, headers: { "token" => user.token }
          response_body = JSON.parse(response.body)
          expect(response_body["message"]).to eq("Could not find user")
        end

        it 'does not return user details' do
          get '/v1/users/details', params: { identifier: 'invalid_username' }, headers: { "token" => user.token }
          response_body = JSON.parse(response.body)
          expect(response_body["username"]).to eq(nil)
        end
      end

      context 'when valid user identifier provided' do
        it 'returns user details' do
          get '/v1/users/details', params: { identifier: user.username }, headers: { "token" => user.token }
          response_body = JSON.parse(response.body)
          expect(response_body["username"]).to eq(user.username)
        end
      end
    end
  end

  describe 'GET /v2/users/reset' do
    let(:user) { create(:user) }

    context 'when no token provided' do
      it 'does not return new token' do
        get '/v1/users/reset'
        response_body = JSON.parse(response.body)
        expect(response_body["token"]).to eq(nil)
      end

      it 'returns an error message' do
        get '/v1/users/reset'
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Invalid token")
      end
    end

    context 'when expired token provided (i.e. more than 2 hours old)' do
      let(:user) { create(:user, last_token_created_at: Time.now - 3.hours) }

      it 'does not return new token' do
        get '/v1/users/reset', params: { identifier: user.username }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["token"]).to eq(nil)
      end

      it 'returns an error message' do
        get '/v1/users/reset', params: { identifier: user.username }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Expired token")
      end
    end

    context 'when authenticated (i.e. valid token provided)' do
      it 'returns new token' do
        get '/v1/users/reset', params: { identifier: user.username }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["token"]).to eq(user.reload.token)
      end
    end
  end

  describe 'PUT /v1/users/assign' do
    let(:user) { create(:user) }
    let(:group) { create(:group) }

    context 'when no token provided' do
      it 'does not assign user to group' do
        put '/v1/users/assign', params: { identifier: user.username, group_name: group.name }
        expect(user.groups).to_not include(group)
      end

      it 'returns error message' do
        put '/v1/users/assign', params: { identifier: user.username, group_name: group.name }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Invalid token")
      end
    end

    context 'when expired token provided (i.e. more than 1 hours old)' do
      let(:user) { create(:user, last_token_created_at: Time.now - 2.hours ) }

      it 'does not assign user to group' do
        put '/v1/users/assign', params: { identifier: user.username, group_name: group.name }, headers: { "token" => user.token }
        expect(user.groups).to_not include(group)
      end

      it 'returns error message' do
        put '/v1/users/assign', params: { identifier: user.username, group_name: group.name }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Expired token")
      end
    end

    context 'when authenticated (i.e. valid token provided)' do
      context 'when user is not an \'admin\' user' do
        it 'assigns user to group' do
          put '/v1/users/assign', params: { identifier: user.username, group_name: group.name }, headers: { "token" => user.token }
          expect(user.groups).to_not include(group)
        end
      end
      context 'when user is an \'admin\' user' do
        let(:user) { create(:user, admin: true) }

        context 'when invalid user indentifier provided' do
          it 'returns a error message' do
            put '/v1/users/assign', params: { identifier: 'incorrect_username', group_name: group.name }, headers: { "token" => user.token }
            response_body = JSON.parse(response.body)
            expect(response_body["message"]).to eq("Could not find user")
          end

          it 'does not assign user to group' do
            put '/v1/users/assign', params: { identifier: 'incorrect_username', group_name: group.name }, headers: { "token" => user.token }
            expect(user.groups).to_not include(group)
          end
        end
        context 'when invalid group name provided' do
          it 'returns a error message' do
            put '/v1/users/assign', params: { identifier: user.username, group_name: 'incorrect_group_name' }, headers: { "token" => user.token }
            response_body = JSON.parse(response.body)
            expect(response_body["message"]).to eq("Could not find group")
          end

          it 'does not assign user to group' do
            put '/v1/users/assign', params: { identifier: user.username, group_name: 'incorrect_group_name' }, headers: { "token" => user.token }
            expect(user.groups).to_not include(group)
          end
        end

        context 'when valid user and group paramaters provided' do
          it 'assigns user to group' do
            put '/v1/users/assign', params: { identifier: user.username, group_name: group.name }, headers: { "token" => user.token }
            expect(user.groups).to include(group)
          end
        end
      end
    end
  end

end
