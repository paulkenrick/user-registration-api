require 'rails_helper'

RSpec.describe 'User', type: :request do

  describe 'POST /v1/groups' do
    let(:user) { create(:user) }

    context 'when no token provided' do
      it 'returns an error message' do
        post '/v1/groups', params: { name: "New Group" }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Invalid token")
      end

      it 'does not create a group' do
        post '/v1/groups', params: { name: "New Group" }
        expect(Group.count).to eq(0)
      end
    end

    context 'when expired token provided (i.e. more than 1 hour old)' do
      let(:user) { create(:user, last_token_created_at: Time.now - 2.hours) }

      it 'returns an error message' do
        post '/v1/groups', params: { name: "New Group" }, headers: { "token" => user.token }
        response_body = JSON.parse(response.body)
        expect(response_body["message"]).to eq("Expired token")
      end

      it 'does not create a group' do
        post '/v1/groups', params: { name: "New Group" }, headers: { "token" => user.token }
        expect(Group.count).to eq(0)
      end
    end

    context 'when authenticated (i.e. valid token provided)' do
      context 'when user is an admin' do
        let(:user) { create(:user, admin: true) }

        context 'when valid name parameter is provided' do
          it 'saves a new group' do
            post '/v1/groups', params: { name: "New Group" }, headers: { "token" => user.token }
            expect(Group.count).to eq(1)
          end

          it 'returns a success message' do
            post '/v1/groups', params: { name: "New Group" }, headers: { "token" => user.token }
            response_body = JSON.parse(response.body)
            expect(response_body["message"]).to eq("Group created successfully")
          end
        end

        context 'when no name parameter is provided' do
          it 'does not save a new group' do
            post '/v1/groups', headers: { "token" => user.token }
            expect(Group.count).to eq(0)
          end

          it 'returns an error message' do
            post '/v1/groups', headers: { "token" => user.token }
            response_body = JSON.parse(response.body)
            expect(response_body["message"]).to eq("Name can't be blank")
          end
        end
      end

      context 'when user is not an admin' do
        it 'does not save a new group' do
          post '/v1/groups', params: { name: "New Group" }, headers: { "token" => user.token }
          expect(Group.count).to eq(0)
        end

        it 'returns an error message' do
          post '/v1/groups', params: { name: "New Group" }, headers: { "token" => user.token }
          response_body = JSON.parse(response.body)
          expect(response_body["message"]).to eq("Permission denied")
        end
      end
    end
  end

end
